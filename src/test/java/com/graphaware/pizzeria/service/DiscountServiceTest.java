package com.graphaware.pizzeria.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.graphaware.pizzeria.model.Pizza;

/**
 * @author Dmytro Fedonin
 *
 */
public class DiscountServiceTest {

    @Test
    public void calculatePineappleDiscount() {
        String topping = "pineapple";
        DiscountService service = new DiscountService();
        Pizza pizza1 = new Pizza();
        pizza1.setToppings(Collections.singletonList(topping));
        pizza1.setPrice(1.0);
        Pizza pizza2 = new Pizza();
        pizza2.setToppings(Collections.singletonList(""));
        pizza2.setPrice(2.0);
        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(pizza2);
        pizzas.add(pizza1);

        double discount = service.calculatePineappleDiscount(pizzas, 3.0);
        assertEquals(0.2, discount);
    }

    @Test
    public void calculateThreeDiscount() {
        DiscountService service = new DiscountService();
        Pizza pizza1 = new Pizza();
        pizza1.setPrice(1.0);
        Pizza pizza2 = new Pizza();
        pizza2.setPrice(2.0);
        Pizza pizza3 = new Pizza();
        pizza3.setPrice(3.0);
        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(pizza3);
        pizzas.add(pizza2);
        pizzas.add(pizza1);

        double discount = service.calculateThreeDiscount(pizzas);
        assertEquals(1.0, discount);
    }

    @Test
    public void calculateDiscount() {
        String topping = "pineapple";
        DiscountService service = new DiscountService();
        Pizza pizza1 = new Pizza();
        pizza1.setToppings(Collections.singletonList(topping));
        pizza1.setPrice(1.0);
        Pizza pizza2 = new Pizza();
        pizza2.setToppings(Collections.singletonList(""));
        pizza2.setPrice(2.0);
        Pizza pizza3 = new Pizza();
        pizza3.setToppings(Collections.singletonList(""));
        pizza3.setPrice(3.0);
        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(pizza3);
        pizzas.add(pizza2);
        pizzas.add(pizza1);

        double discount = service.calculateDiscount(pizzas, 6.0);
        assertEquals(1.0, discount);
    }

    @Test
    public void calculateDiscount2() {
        String topping = "pineapple";
        DiscountService service = new DiscountService();
        Pizza pizza1 = new Pizza();
        pizza1.setToppings(Collections.singletonList(topping));
        pizza1.setPrice(1.0);
        Pizza pizza2 = new Pizza();
        pizza2.setToppings(Collections.singletonList(""));
        pizza2.setPrice(2.0);
        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(pizza2);
        pizzas.add(pizza1);

        double discount = service.calculateDiscount(pizzas, 3.0);
        assertEquals(0.2, discount);
    }

    @Test
    public void calculateDiscount0() {
        DiscountService service = new DiscountService();
        Pizza pizza2 = new Pizza();
        pizza2.setToppings(Collections.singletonList(""));
        pizza2.setPrice(2.0);
        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(pizza2);

        double discount = service.calculateDiscount(pizzas, 3.0);
        assertEquals(0.0, discount);
    }
}
