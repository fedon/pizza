package com.graphaware.pizzeria.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.graphaware.pizzeria.model.Pizza;

/**
 * @author Dmytro Fedonin
 *
 */
@Service
public class DiscountService {

    /**
     * Discounts are not cumulative. Apply which one is bigger.
     * 
     * @param pizzas
     *            in a purchase.
     * @param price
     *            of all pizza before discount.
     * @return discount value
     */
    public Double calculateDiscount(List<Pizza> pizzas, Double price) {

        Double pineappleDiscount = calculatePineappleDiscount(pizzas, price);
        Double discount = calculateThreeDiscount(pizzas);
        if (pineappleDiscount > discount) {
            discount = pineappleDiscount;
        }
        return discount;
    }

    /**
     * Buy a pineapple pizza, get 10% off the others.
     * 
     * @param pizzas
     *            in a purchase.
     * @param price
     *            of all pizza before discount.
     * @return discount value
     */
    Double calculatePineappleDiscount(List<Pizza> pizzas, Double price) {
        Double pineapplePrice = 0.0;
        for (Pizza pizza : pizzas) {
            if (pizza.getToppings().contains("pineapple")) {
                pineapplePrice += pizza.getPrice();
            }
        }
        if (pineapplePrice == 0.0) {
            return 0.0;
        }

        return (price - pineapplePrice) * 0.1;
    }

    /**
     * If the customer buys 3 pizzas, the cheapest of the 3 is free.
     * 
     * @param pizzas
     *            in a purchase.
     * @return discount value
     */
    Double calculateThreeDiscount(List<Pizza> pizzas) {
        Double minPrice = -1.0;
        if (pizzas.size() == 3) {
            for (Pizza pizza : pizzas) {
                if (minPrice < 0) {
                    minPrice = pizza.getPrice();
                } else {
                    if (minPrice > pizza.getPrice()) {
                        minPrice = pizza.getPrice();
                    }
                }
            }
            return minPrice;
        }

        return 0.0;
    }
}
